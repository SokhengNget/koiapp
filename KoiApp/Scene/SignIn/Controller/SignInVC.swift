import UIKit
import Alamofire

class SignInVC: UIViewController {
    
    let signInView = SignInView()
    
    override func loadView() {
        self.view = signInView
        signInView.signInButton.addTarget(self, action: #selector(signInButtonClick), for: .touchUpInside)
        signInView.backButton.addTarget(self, action: #selector(backButtonClick), for: .touchUpInside)
    }
    
    @objc private func signInButtonClick(){
        let email: String = signInView.emailView.textField.text!
        let password: String = signInView.passwordView.textField.text!

        let modifiedEmail: String = modifiedPhoneNumber(phoneNumber: email)

        if email.isEmpty && password.isEmpty{
            showToast(controller: self, message: "Please input email and password", seconds: 3)
        }
        else if email.isEmpty{
            showToast(controller: self, message: "Please input email", seconds: 3)
        }
        else if password.isEmpty{
            showToast(controller: self, message: "Please input password", seconds: 3)
        }
        else{

            let KEY = "KoiTheCambodia0123456789@Pass0rd"
            let IV = "UjXn2r5u8x/A?D(G"

            let encryptPassword = AESUtils.instance.encryptionAES(value: password, key256: KEY, iv: IV)
//            print(encryptPassword)

            let signInData = SignInModel(phoneNumber: modifiedEmail, password: encryptPassword)

            AF.request(baseUrlAuth + "sign-in",
                       method: .post,
                       parameters: signInData,
                       encoder: JSONParameterEncoder.default).response { response in
                        guard let data = response.data else { return }
//                        print(String.init(data: data, encoding: .utf8) as Any)
                        do{
                            let decodeData = try JSONDecoder().decode(LoginResponse.self, from: data)
                            if decodeData.response.code == 200{
                                let vc = HomeVC()
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            else{
                                print(decodeData.response.message)
                            }
                        }catch{
                            print(error)
                        }
            }
        }
    }
    
    @objc private func backButtonClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showToast(controller: UIViewController, message : String, seconds: Double){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = .black
        alert.view.alpha = 0.5
        alert.view.layer.cornerRadius = 15
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        controller.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    func modifiedPhoneNumber(phoneNumber: String) -> String{
        var number: String
        number = String(phoneNumber.dropFirst())
        let modifiedNumber: String = "+855" + number
        return modifiedNumber
    }
    
}
