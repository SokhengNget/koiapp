import UIKit

struct SignInModel: Codable {
    let phoneNumber: String
    let password: String
}

let baseUrlAuth = "http://uat-api.karanakkoithe.com/api/v1/life-style/auth/"

struct LoginResponse: Codable{
    let response: Response
    let data: Data
}

struct Response: Codable{
    let code: Int
    let message: String
}

struct Data: Codable{
    let accessToken: String
    let tokenType: String
}


