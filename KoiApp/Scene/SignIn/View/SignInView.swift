import UIKit

class SignInView: UIView {
    
    lazy var backgroundImage: UIImageView = {
        let image = UIImageView(image: UIImage(named: "Rectangle_327"))
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(UIImage(named: "ic_back_arrow_black"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var signInTitleBar: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-SemiBold", size: 17)
        label.text = "SIGN IN"
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var firstStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [backButton, signInTitleBar])
        sv.axis = .horizontal
        sv.alignment = .leading
        sv.distribution = .fill
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    lazy var emailView: CustomTextField = {
        let emailTextFiedl = CustomTextField()
        emailTextFiedl.textLabel.font = UIFont(name: "Montserrat-Regular", size: 14)
        emailTextFiedl.textLabel.text = "Email/Tel"
        emailTextFiedl.textField.font = UIFont(name: "Montserrat-Regular", size: 14)
        emailTextFiedl.textField.placeholder = "Email/Tel"
        emailTextFiedl.textField.textAlignment = .left
        emailTextFiedl.translatesAutoresizingMaskIntoConstraints = false
        return emailTextFiedl
    }()
    
    lazy var passwordView: CustomTextField = {
        let passwordTextFiedl = CustomTextField()
        passwordTextFiedl.textLabel.font = UIFont(name: "Montserrat-Regular", size: 14)
        passwordTextFiedl.textLabel.text = "Password"
        passwordTextFiedl.textField.font = UIFont(name: "Montserrat-Regular", size: 14)
        passwordTextFiedl.textField.placeholder = "Password"
        passwordTextFiedl.textField.isSecureTextEntry = true
        passwordTextFiedl.textField.textAlignment = .left
        passwordTextFiedl.translatesAutoresizingMaskIntoConstraints = false
        return passwordTextFiedl
    }()
    
    lazy var secondStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [emailView, passwordView])
        sv.axis = .vertical
        sv.alignment = .fill
        sv.distribution = .fillProportionally
        sv.spacing = 25
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    lazy var signInButton: CustomButton = {
        let button = CustomButton(type: .system)
        button.backgroundColor = #colorLiteral(red: 0.8540286422, green: 0.6520106196, blue: 0.2639814019, alpha: 1)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.setTitle("SIGN IN", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var forgetPasswordButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .none
        button.setTitleColor(.black, for: .normal)
        button.setTitle("Forget your password?", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-Medium", size: 12)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var accountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-Regular", size: 12)
        label.text = "Don't have account?"
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var createAccountButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .none
        button.setTitleColor(.black, for: .normal)
        button.setTitle("Create One", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-Medium", size: 12)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var accountStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [accountLabel, createAccountButton])
        sv.axis = .horizontal
        sv.alignment = .center
        sv.distribution = .fill
        sv.spacing = 5
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    lazy var thirdStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [forgetPasswordButton, accountStack])
        sv.axis = .vertical
        sv.alignment = .center
        sv.distribution = .fill
        sv.spacing = -5
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    func setupView(){
        addSubview(backgroundImage)
        addSubview(firstStack)
        addSubview(secondStack)
        addSubview(signInButton)
        addSubview(thirdStack)
        NSLayoutConstraint.activate([
            backgroundImage.topAnchor.constraint(equalTo: topAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            firstStack.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            firstStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            firstStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            
            backButton.heightAnchor.constraint(equalToConstant: 15),
            backButton.widthAnchor.constraint(equalToConstant: 15),
            backButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
                        
//            emailTextField.widthAnchor.constraint(equalToConstant: 250),
//            passwordTextField.widthAnchor.constraint(equalToConstant: 250),
            
            emailView.heightAnchor.constraint(equalToConstant: 20),
            passwordView.heightAnchor.constraint(equalToConstant: 20),
            
//            bottomLineEmailView.heightAnchor.constraint(equalToConstant: 1),
//            bottomLinePasswordView.heightAnchor.constraint(equalToConstant: 1),
            
            secondStack.topAnchor.constraint(equalTo: firstStack.bottomAnchor, constant: 100),
            secondStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 40),
            secondStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40),
//            secondStack.heightAnchor.constraint(equalToConstant: 100),
            
            signInButton.topAnchor.constraint(equalTo: secondStack.bottomAnchor, constant: 50),
            signInButton.heightAnchor.constraint(equalToConstant: 35),
            signInButton.widthAnchor.constraint(equalTo: backgroundImage.widthAnchor, multiplier: 0.6),
            signInButton.centerXAnchor.constraint(equalTo: backgroundImage.centerXAnchor),
            
            thirdStack.topAnchor.constraint(equalTo: signInButton.bottomAnchor, constant: 15),
            thirdStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 70),
            thirdStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -70)
        ])
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
