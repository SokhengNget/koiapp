import UIKit

class WelcomeVC: UIViewController, UIGestureRecognizerDelegate {
    
    let welcomeView = WelcomeView()
    
    override func loadView() {
        self.view = welcomeView
        welcomeView.signInButton.addTarget(self, action: #selector(signInClick), for: .touchUpInside)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

    func setBackgroundImage(){
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "bg-authentication")
        backgroundImage.contentMode =  UIView.ContentMode.scaleAspectFill
        view.insertSubview(backgroundImage, at: 0)
    }
    
    @objc func signInClick(){
        let vc = SignInVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
