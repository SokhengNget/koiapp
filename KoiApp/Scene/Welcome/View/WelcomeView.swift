import UIKit

class WelcomeView: UIView{
    
    lazy var backgroundImage: UIImageView = {
        let image = UIImageView(image: UIImage(named: "bg-authentication"))
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var welcomeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-Medium", size: 50)
        label.text = "Welcome"
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-Regular", size: 12)
        label.text = "Sign up KOI The Membership Program to receive updates, collect point, and get exclusive member benefits."
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var signInButton: CustomButton = {
        let button = CustomButton(type: .system)
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.setTitle("SIGN IN", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var signUpButton: CustomButton = {
        let button = CustomButton(type: .system)
        button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.setTitle("SIGN UP", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var facebookButton: CustomButton = {
        let button = CustomButton(type: .system)
        button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        button.setTitleColor(#colorLiteral(red: 0.1185043231, green: 0.3769358397, blue: 0.5903427005, alpha: 1), for: .normal)
        button.setTitle("CONNECT WITH FACEBOOK", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        let icon = UIImage(named: "fb-icon")?.withRenderingMode(.alwaysOriginal)
        button.setImage(icon, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.imageEdgeInsets = UIEdgeInsets(top: 3, left: -5, bottom: 0, right: 0)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var googleButton: CustomButton = {
        let button = CustomButton(type: .system)
        button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        button.setTitleColor(#colorLiteral(red: 0.8489566445, green: 0.1369232237, blue: 0.1631815135, alpha: 1), for: .normal)
        button.setTitle("CONNECT WITH GOOGLE", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        let icon = UIImage(named: "fb-icon")?.withRenderingMode(.alwaysOriginal)
        button.setImage(icon, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.imageEdgeInsets = UIEdgeInsets(top: 3, left: -5, bottom: 0, right: 0)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var firstStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [welcomeLabel, descriptionLabel])
        sv.alignment = .center
        sv.axis = .vertical
        sv.distribution = .fill
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    lazy var secondStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [signInButton, signUpButton, facebookButton, googleButton])
        sv.alignment = .fill
        sv.axis = .vertical
        sv.distribution = .fill
        sv.spacing = 12
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    lazy var mainStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [firstStack, secondStack])
        sv.alignment = .center
        sv.axis = .vertical
        sv.distribution = .fill
        sv.spacing = 40
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    func setupView(){
        addSubview(backgroundImage)
        addSubview(mainStack)
        NSLayoutConstraint.activate([
            backgroundImage.topAnchor.constraint(equalTo: topAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            mainStack.topAnchor.constraint(equalTo: topAnchor, constant: 180),
            mainStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            mainStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            
            secondStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            secondStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            
            signInButton.heightAnchor.constraint(equalToConstant: 40),
            
            signUpButton.heightAnchor.constraint(equalTo: signInButton.heightAnchor, multiplier: 1.0),
            
            facebookButton.heightAnchor.constraint(equalTo: signInButton.heightAnchor, multiplier: 1.0),
            
            googleButton.heightAnchor.constraint(equalTo: signInButton.heightAnchor, multiplier: 1.0),
        ])
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
