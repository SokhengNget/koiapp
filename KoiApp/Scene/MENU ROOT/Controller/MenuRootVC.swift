import UIKit

class MenuRootVC: UIViewController {
    
    lazy var tabButtonView = TabButtonView()
    var collectionButton:[UIButton]!
    
    lazy var koiViewController = HomeVC()
    lazy var cardViewController = CardVC()
    
    override func loadView() {
        super.loadView()
        self.view = tabButtonView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaultView()
        setupEvent()
    }
    
    fileprivate func setupDefaultView(){
        collectionButton = [tabButtonView.koiButton, tabButtonView.cardButton, tabButtonView.outletButton, tabButtonView.orderButton]
    }
    
    fileprivate func setupEvent(){
        tabButtonView.koiButton.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        tabButtonView.koiButton.isSelected = true
        add(asChildViewController: koiViewController)
        collectionButton.forEach { (allButton) in
            allButton.addTarget(self, action: #selector(onTopMenuClick), for: .touchUpInside)
        }
    }
    
    @objc func onTopMenuClick(_ sender:UIButton){

        collectionButton.forEach {
            $0.isSelected = false
            $0.backgroundColor = .none
        }

        collectionButton[sender.tag].backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        collectionButton[sender.tag].isSelected = true

        switch sender.tag {
        case 0:
            add(asChildViewController: koiViewController)
        case 1:
            add(asChildViewController: cardViewController)
        case 2:
            print("Outlet")
        case 3:
            print("Order")
        default:
            print("nothing")
        }
    }
    
    
    private func add(asChildViewController viewController:UIViewController){
        //Add Child View Controller
        addChild(viewController)
        //Add Child View as Subview
        tabButtonView.containerView.addSubview(viewController.view)
        //Configure Child View
        viewController.view.frame = tabButtonView.containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        //Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController:UIViewController){
        //Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        //Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        //Notify Child View Controller
        viewController.removeFromParent()
    }
    
}
