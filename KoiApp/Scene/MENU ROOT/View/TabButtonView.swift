import UIKit

class TabButtonView: UIView {
    
    lazy var koiButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.setTitle("KOI", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        button.isSelected = false
        button.layer.cornerRadius = 15
        button.layer.masksToBounds = true
        button.tag = 0
        button.backgroundColor = .clear
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var cardButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.setTitle("CARD", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        button.isSelected = false
        button.layer.cornerRadius = 15
        button.layer.masksToBounds = true
        button.tag = 1
        button.backgroundColor = .clear
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var outletButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.setTitle("OUTLETS", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        button.isSelected = false
        button.layer.cornerRadius = 15
        button.layer.masksToBounds = true
        button.tag = 2
        button.backgroundColor = .clear
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var orderButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.setTitle("ORDER", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        button.isSelected = false
        button.layer.cornerRadius = 15
        button.layer.masksToBounds = true
        button.tag = 3
        button.backgroundColor = .clear
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var menuStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [koiButton, cardButton, outletButton, orderButton])
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .fillEqually
        sv.spacing = 5
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    lazy var containerView: UIView = {
        let subView = UIView()
        subView.backgroundColor = .clear
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
    }()
    
    lazy var containerSubMenuView: UIView = {
        let subView = UIView()
        subView.layer.cornerRadius = 15
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
    }()
    
    fileprivate func setupView(){
        addSubview(containerView)
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        addSubview(containerSubMenuView)
        NSLayoutConstraint.activate([
            containerSubMenuView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            containerSubMenuView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerSubMenuView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerSubMenuView.heightAnchor.constraint(equalToConstant: 30)
        ])
        
        containerSubMenuView.addSubview(menuStack)
        NSLayoutConstraint.activate([
            menuStack.topAnchor.constraint(equalTo: containerSubMenuView.topAnchor),
            menuStack.leadingAnchor.constraint(equalTo: containerSubMenuView.leadingAnchor, constant: 2),
            menuStack.trailingAnchor.constraint(equalTo: containerSubMenuView.trailingAnchor, constant: -2),
            menuStack.bottomAnchor.constraint(equalTo: containerSubMenuView.bottomAnchor)
        ])
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
