import UIKit

class HomeView: UIView {
    
    lazy var backgroundImage: UIImageView = {
        let image = UIImageView(image: UIImage(named: "Rectangle_327"))
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var circleImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "circle_icon"))
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-SemiBold", size: 30)
        label.text = "Taerak"
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var accountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-Regular", size: 15)
        label.text = "Account & Setting"
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var secondStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [usernameLabel, accountLabel])
        sv.axis = .vertical
        sv.alignment = .center
        sv.distribution = .fill
        sv.spacing = 3
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    lazy var leftView: RewardView = {
        let view = RewardView()
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 15
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var rightView: BalanceView = {
        let view = BalanceView()
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 15
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var splitLineView: UIView = {
        let subView = UIView()
        subView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
    }()
    
    lazy var rewardStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [leftView, rightView])
        sv.axis = .horizontal
        sv.alignment = .center
        sv.distribution = .fillEqually
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    lazy var rewardView: UIView = {
        let subView = UIView()
        subView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        subView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        subView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        subView.layer.shadowOpacity = 1.0
        subView.layer.shadowRadius = 0.0
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
    }()
    
    lazy var inboxButton: CustomButton = {
        let button = CustomButton(type: .system)
        button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.setTitle("INBOX", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var accountButton: CustomButton = {
        let button = CustomButton(type: .system)
        button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.setTitle("ACCOUNT", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var newsButton: CustomButton = {
        let button = CustomButton(type: .system)
        button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        button.setTitle("NEWS AND PROMOTIONS", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var buttonStack: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [inboxButton, accountButton, newsButton])
        sv.axis = .vertical
        sv.alignment = .fill
        sv.distribution = .fill
        sv.spacing = 10
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    func setupview(){
        addSubview(backgroundImage)
        addSubview(circleImageView)
        addSubview(secondStack)
        rewardStack.addSubview(splitLineView)
        rewardView.addSubview(rewardStack)
        addSubview(rewardView)
        addSubview(buttonStack)
        NSLayoutConstraint.activate([
            backgroundImage.topAnchor.constraint(equalTo: topAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: bottomAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            circleImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 180),
            circleImageView.widthAnchor.constraint(equalTo: backgroundImage.widthAnchor, multiplier: 0.6),
            circleImageView.centerXAnchor.constraint(equalTo: backgroundImage.centerXAnchor),
            
            secondStack.topAnchor.constraint(equalTo: circleImageView.bottomAnchor, constant: 30),
            secondStack.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            leftView.heightAnchor.constraint(equalToConstant: 100),
            rightView.heightAnchor.constraint(equalToConstant: 100),
            
            splitLineView.widthAnchor.constraint(equalToConstant: 1),
            splitLineView.topAnchor.constraint(equalTo: rewardStack.topAnchor, constant: 5),
            splitLineView.bottomAnchor.constraint(equalTo: rewardStack.bottomAnchor, constant: -5),
            splitLineView.centerXAnchor.constraint(equalTo: rewardStack.centerXAnchor),
            
            rewardStack.topAnchor.constraint(equalTo: rewardView.topAnchor),
            rewardStack.leadingAnchor.constraint(equalTo: rewardView.leadingAnchor),
            rewardStack.trailingAnchor.constraint(equalTo: rewardView.trailingAnchor),
            
            rewardView.topAnchor.constraint(equalTo: secondStack.bottomAnchor, constant: 20),
            rewardView.leadingAnchor.constraint(equalTo: backgroundImage.leadingAnchor, constant: 10),
            rewardView.trailingAnchor.constraint(equalTo: backgroundImage.trailingAnchor, constant: -10),
            
            inboxButton.heightAnchor.constraint(equalToConstant: 35),
            accountButton.heightAnchor.constraint(equalTo: inboxButton.heightAnchor, multiplier: 1.0),
            newsButton.heightAnchor.constraint(equalTo: inboxButton.heightAnchor, multiplier: 1.0),
            
            buttonStack.topAnchor.constraint(equalTo: rewardStack.bottomAnchor, constant: 20),
            buttonStack.leadingAnchor.constraint(equalTo: backgroundImage.leadingAnchor, constant: 10),
            buttonStack.trailingAnchor.constraint(equalTo: backgroundImage.trailingAnchor, constant: -10)
        ])
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class RewardView: UIView {
    
    lazy var rewardLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-SemiBold", size: 14)
        label.text = "REWARD\nPOINT"
        label.numberOfLines = 0
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var claimLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-SemiBold", size: 12)
        label.text = "Claim\nawesome\ngifts"
        label.numberOfLines = 0
        label.textColor = #colorLiteral(red: 0.7960448265, green: 0.7999623418, blue: 0.7958258986, alpha: 1)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var pointLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-SemiBold", size: 20)
        label.text = "168"
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    
    func setupView(){
        addSubview(rewardLabel)
        addSubview(claimLabel)
        addSubview(pointLabel)
        NSLayoutConstraint.activate([
            rewardLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            rewardLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            
            claimLabel.topAnchor.constraint(equalTo: rewardLabel.bottomAnchor, constant: 1),
            claimLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            claimLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5),
            
            pointLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5),
            pointLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
        ])
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class BalanceView: UIView {
    
    lazy var accountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-SemiBold", size: 14)
        label.text = "ACCOUNT\nBALANCE"
        label.numberOfLines = 0
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var expireLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-SemiBold", size: 12)
        label.text = "Expires on\nNov 29, 2019"
        label.numberOfLines = 0
        label.textColor = #colorLiteral(red: 0.7960448265, green: 0.7999623418, blue: 0.7958258986, alpha: 1)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var amountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-SemiBold", size: 20)
        label.text = "$0.00"
        label.textColor = #colorLiteral(red: 0.8342693448, green: 0.6363088489, blue: 0.2566933632, alpha: 1)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setupView(){
        addSubview(accountLabel)
        addSubview(expireLabel)
        addSubview(amountLabel)
        NSLayoutConstraint.activate([
            accountLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            accountLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            
            expireLabel.topAnchor.constraint(equalTo: accountLabel.bottomAnchor, constant: 3),
            expireLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            
            amountLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5),
            amountLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10)
            
        ])
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
