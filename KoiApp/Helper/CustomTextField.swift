import UIKit

class CustomTextField: UIView {
    
    lazy var textLabel: UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var textField: UITextField = {
        let textfield = UITextField()
        textfield.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
    
    lazy var bottomLineView: UIView = {
        let subView = UIView()
        subView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        subView.translatesAutoresizingMaskIntoConstraints = false
        return subView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    func setupView(){
        addSubview(textLabel)
        addSubview(textField)
        addSubview(bottomLineView)
        NSLayoutConstraint.activate([
            textLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            textLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            
            textField.centerYAnchor.constraint(equalTo: centerYAnchor),
            textField.leadingAnchor.constraint(equalTo: textLabel.trailingAnchor, constant: 10),
            textField.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            bottomLineView.topAnchor.constraint(equalTo: textLabel.topAnchor, constant: 30),
            bottomLineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            bottomLineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            bottomLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
